const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe('Testing mylib', () => {

    before(() => {
        ending = 'Test is finished';
        console.log('Test is started');
    });

    it("The Function addition() should return 2 when function`s parameters are a=1, b=1", () => {
        const result = mylib.add(1, 1);
        expect(result).to.equal(2);
    });

    it("The Function substraction() should return 0 when function`s parameters are a=1, b=1", () => {
        const result = mylib.sub(1, 1);
        expect(result).to.equal(0);
    });

    it("The Function multiplication() should return 4 when function`s parameters are a=2, b=2", () => {
        const result = mylib.multiply(2, 2);
        expect(result).to.equal(4);
    });

    it('The Function division() should throw an error when dividing by zero', () => {
        let success = false;
        try {
            const result = mylib.divide(6, 0);
            success = true;
        } catch(err) {
            expect(success).to.equal(false);
        }
    });
    it('The Function division() should return 3 when function`s parameters are a=6, b=2', () => {
        const result = mylib.divide(6, 2);
        expect(result).to.equal(3);
    });

    after(() => {
        console.log(ending);
    });
});