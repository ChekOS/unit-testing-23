const express = require('express');
const bodyParser = require('body-parser');
const mylib = require("./mylib");
const app = express();

const PORT = 3000;

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Main endpoint');
});

app.get('/addition', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = mylib.add(a,b);
    res.send(sum.toString());
});
app.get('/substraction', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const sub = mylib.sub(a,b);
  res.send(sub.toString());
});
app.get('/multiplication', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const mult = mylib.multiply(a,b);
  res.send(mult.toString());
});
app.get('/division', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  const div = mylib.divide(a, b);
  res.send(div.toString());
});

app.listen(PORT, () => {
  console.log(`Server is running on: http://localhost:${PORT}`)
});

module.exports = app.listen();