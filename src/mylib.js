const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

/**
 * This function returns sum of two parameters
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const addition =(param1, param2) => {
    const result = param1 + param2;
    return result;
}

/**
 * This function returns result of subtraction of two parameters.
 * @param {number} param1
 * @param {number} param2
 * @returns 
 */
const substraction = (param1 ,param2) => {
    const result = param1 - param2;
    return result;
};

/**
 * This function  return result of multiplication of two parameters.
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const multiplication =(param1, param2) => {
    const result = param1 * param2;
    return result;
}

/**
 * This function returns result of division of two parameters.
 * @param {number} param1 
 * @param {number} param2 
 * @returns {number}
 */
const division = (param1, param2) => {
    if (param2 === 0) {
        throw new Error("Division by zero is prohibited!");
    } else {
        const result = param1 / param2;
        return result;
    }
}

module.exports = {
    add:addition,
    sub:substraction,
    multiply: multiplication,
    divide:division
}
